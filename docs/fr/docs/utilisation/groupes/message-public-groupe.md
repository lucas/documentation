# Message public d'un groupe

Un endroit pour publier quelque chose à l'intention du monde entier, de votre communauté ou simplement des membres de votre groupe.

## Créer un nouveau billet

!!! note
    Vous devez être modérateur ou administratrice pour créer un billet (voir [rôles](../../groupes/roles-groupe/)).

Pour créer un nouveau billet de groupe, vous devez, sur la page de gestion de votre groupe, cliquer sur le bouton **+ Poster un message public**.

![image de la liste des message public](../../images/group-public-post-FR.png)

Vous devez ensuite&nbsp;:

  1. ajouter une image d'illustration
  * ajouter un titre (requis)
  * ajouter quelques tags
  * ajouter un texte de billet (requis)
  * paramétrer **qui peut voir ce billet**&nbsp;:
    * **Visible partout sur le web**
    * **Accessible uniquement par le lien**
    * **Accessible uniquement aux membres du groupe**
  * cliquer sur le bouton **Publier** (ou annuler)

![image d'un billet vide](../../images/group-add-new-post-template-empty-FR.png)

## Modifier un billet

!!! note
    Vous devez être modérateur ou administratrice pour modifier un billet (voir [rôles](../../groupes/roles-groupe/)).

Pour modifier un billet vous devez être connecté⋅e à votre compte et, sur la page du billet, cliquer sur le lien **Modifier** (sous la date)&nbsp;:

![image montrant le lien pour modifier](../../images/group-post-edit-link-FR.png)

Une fois les modifications faites vous devez cliquer sur le bouton **Mettre à jour le billet**.

## Supprimer son billet de groupe

!!! note
    Vous devez être modérateur ou administratrice pour supprimer un billet (voir [rôles](../../groupes/roles-groupe/)).

Pour supprimer un billet vous devez être connecté⋅e à votre compte et, sur la page du billet, cliquer sur le lien **Modifier** (sous la date)&nbsp; et cliquer sur le bouton **Supprimer le billet**.
