# Comment gérer la modération

!!! info
    Vous devez être modérateur⋅trice ou administrateur⋅trice pour pouvoir accéder à cette section

Cette documentation a pour but de vous montrer comment gérer du côté administration les signalements [de commentaires](../../evenements/commentaires-evenement/#signaler-un-commentaire) et [d'événements](../../evenements/actions-evenement/#signalement) fait par les profils.

## Page des signalements

!!! note
    Lors d'un signalement les modérateurs et administratrices reçoivent un mail de notification.

Vous pouvez accéder à cette page en cliquant&nbsp;:

  1. sur sur votre avatar
  * sur **Administration**
  * **Signalements** dans le menu latéral gauche sous la section **Modération**

Vous avez la possibilité de filtrer selon le statut **Ouvert**, **Résolu** ou **Fermé**&nbsp;:

![imgae vue liste signalements](../../images/moderator-reports-list-FR.png)

### Signalement de commentaire

Quand un nouveau signalement est disponible vous pouvez l'ouvrir en cliquant dessus&nbsp;:

![vue d'un signalement de commentaire](../../images/moderator-report-comment-view-annoted-FR.png)

  1. bouton **Marquer comme résolu**&nbsp;: permet de marquer le signalement comme accepté et traité
  * bouton **Fermé**&nbsp;: permet de fermer directement le signalement
  * bouton **Supprimer** pour supprimer **l'événement**
  * commentaire fait par la personne qui **signale** le commentaire
  * commentaire **signalé**
  * bouton pour **supprimer le commentaire signalé**

### Signalement d'un événement

Quand un nouveau signalement est disponible vous pouvez l'ouvrir en cliquant dessus&nbsp;:

![image d'un signalement annoté](../../images/moderator-report-event-view-annoted-FR.png)

  1. bouton **Marquer comme résolu**&nbsp;: permet de marquer le signalement comme accepté et traité
  * bouton **Fermé**&nbsp;: permet de fermer directement le signalement
  * commentaire du profil qui signale en quoi cet événement est à signaler
  * événement **signalé**
  * bouton **Supprimer** pour supprimer **l'événement**

### Discussion de modération

Les modératrices peuvent discuter entre elles au sujet des signalements en utilisant les **Notes**&nbsp;:

![section notes](../../images/report-notes-FR.png)

## Journaux de modération

Cette section permet de visualiser les actions de modérations effectuées par l'équipe de modération.

## Utilisateur⋅ice⋅s

Cette section liste tous les comptes créés sur l'instance. Vous pouvez lister directement tous les profils d'un compte en cliquant sur **>** près de l'adresse mail&nbsp;:

![image liste des profils d'un compte](../../images/moderator-users-list-profiles-FR.png)

En cliquant sur l'adresse mail vous arrivez sur une fiche plus complète&nbsp;:

![image fiche utilisatrices](../../images/moderator-users-profiles-FR.png)

Avec cette vue il est possible de **suspendre** un compte et ses profils associés.

!!! danger
    Cliquer sur le bouton **Suspendre** rend immédiatement inutilisable un compte et cette action est **irréversible**&nbsp;!

## Profils

Cette section liste tous les **profils** créés sur l'instance. Il est possible de les lister selon&nbsp;:

  * si ils sont locaux (sur l'instance) ou non
  * si ils sont suspendus ou non
  * leur domaine d'instance distante et fédérée (filtrer les comptes de l'instance `mobilizon.fr` par exemple)


![image filtre des profils](../../images/moderator-profiles-filter-FR.png)

![image d'un profil vu depuis la modération](../../images/moderator-rose-utopia-profile-view-FR.png)


## Groupes

Cette section liste tous les **groupes** créés sur l'instance. Il est possible de les lister selon&nbsp;:

  * si ils sont locaux (sur l'instance) ou non
  * si ils sont suspendus ou non
  * leur domaine d'instance distante et fédérée (filtrer les comptes de l'instance `mobilizon.fr` par exemple)

![](../../images/moderator-group-list-FR.png)

En cliquant sur un groupe en particulier vous obtenez de plus amples informations&nbsp;:

![](../../images/moderator-group-view-FR.png)

Avec cette vue il est possible de **suspendre** un groupe et ses événements associés.

!!! danger
    Cliquer sur le bouton **Suspendre** rend immédiatement inutilisable un groupe et cette action est **irréversible**&nbsp;!
