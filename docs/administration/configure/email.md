# Email

Mobilizon requires a SMTP server to deliver emails. Using 3rd-party mail providers (Mandrill, SendGrid, Mailjet, …) will be possible in the future.

## SMTP configuration

Mobilizon default settings assumes a SMTP server listens on `localhost`, port `25`. To specify a specific server and credentials, you can add the following section in your config file and modify credentials to your needs.

```elixir
config :mobilizon, Mobilizon.Web.Email.Mailer,
  adapter: Swoosh.Adapters.SMTP,
  relay: "localhost",
  # usually 25, 465 or 587
  port: 25,
  username: "",
  password: "",
  # can be `:always` or `:never`
  tls: :if_available,
  allowed_tls_versions: [:tlsv1, :"tlsv1.1", :"tlsv1.2"],
  retries: 1,
  # can be `true`
  no_mx_lookups: false,
  # can be `:always`. If your smtp relay requires authentication set it to `:always`.
  auth: :if_available
```

You'll need to restart Mobilizon to apply the new settings.